// settexpreamble.asy  Set up a common texpreamble
// Use in your final mygraphic.asy as:
//   cd("../../../asy/");
//   import settexpreamble;
//   cd("");
//   settexpreamble();

string settexpreamble() {
  // Get the current directory
  string current_dir = cd("");
  int project_part_of_path_dex = rfind(current_dir, "/abstract_algebra/");
  string path_prefix = substr(current_dir, 0, project_part_of_path_dex);
  // write(stdout, "Path prefix is "+path_prefix+"<-- ");
  texpreamble("\usepackage{"+path_prefix+"/abstract_algebra/src/stubs/tex/presentationfonts}\usepackage{usualjh}");
  return(path_prefix);
}
