// group.asy  Draw figures for group presentation

cd("../../stubs/asy");
import settexpreamble;
cd("");
settexpreamble();

import fontsize;
defaultpen(fontsize(9.24994pt));
import texcolors;

import settings;
settings.outformat="pdf";

// Name of output files
string OUTPUT_FN = "group%03d";


path equilateral_triangle(real sidelength) {
  real altitude = sidelength*Cos(30);  // capital letters make it take degrees
  pair a=(-0.5*sidelength,-0.5*altitude);
  pair b=(0.5*sidelength,-0.5*altitude);
  pair c=(0,0.5*altitude);
  return(a--b--c--cycle);
}

pair triangle_centroid(path t) {
  pair a=point(t,0);
  pair b=point(t,1);
  pair c=point(t,2);
  return(0.5*(a.x+b.x+c.x, a.y+b.y+c.y));
}

// This gives the point to rotate around
pair triangle_center(real sidelength) {
  real altitude = sidelength*Cos(30);  // capital letters make it take degrees
  return( (0, Tan(30)*0.5*sidelength)-(0,0.5*altitude) );
}

path square_path(real sidelength) {
  return( (-0.5*sidelength,0.5*sidelength)
	  --(0.5*sidelength,0.5*sidelength)
	  --(0.5*sidelength,-0.5*sidelength)
	  --(-0.5*sidelength,-0.5*sidelength)--cycle);
}

pen p = currentpen+extendcap+miterjoin;
real sidelength = 0.85;
path triangle = equilateral_triangle(sidelength);
pair center = triangle_center(sidelength);



// ========= Equilateral triangle =========================
picture pic;
int picnum = 0;
unitsize(pic,1cm);


// Draw the triangle
filldraw(pic, triangle, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(triangle, 2), 2S, p);
label(pic, "{\tiny $b$}", point(triangle, 1), 2*unit(-point(triangle, 1)), p);
label(pic, "{\tiny $c$}", point(triangle, 0), 2*unit(-point(triangle, 0)), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(triangle, 2), 1*N, p);
label(pic, "{\tiny $b$}", point(triangle, 1), 1*unit(point(triangle, 1)), p);
label(pic, "{\tiny $c$}", point(triangle, 0), 1*unit(point(triangle, 0)), p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ========= turn clockwise =========================
picture pic;
int picnum = 1;
unitsize(pic,1cm);

// Draw the triangle
path transformed_triangle = rotate(-120, center)* triangle;  // -120 is clockwise
filldraw(pic, transformed_triangle, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_triangle, 2), 2*unit(-point(transformed_triangle, 2)), p);
label(pic, "{\tiny $b$}", point(transformed_triangle, 1), 2*unit(-point(transformed_triangle, 1)), p);
label(pic, "{\tiny $c$}", point(transformed_triangle, 0), 2*unit(-point(transformed_triangle, 0)), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(triangle, 2), 1*N, p);
label(pic, "{\tiny $b$}", point(triangle, 1), 1*unit(point(triangle, 1)), p);
label(pic, "{\tiny $c$}", point(triangle, 0), 1*unit(point(triangle, 0)), p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ========= t^2 =========================
picture pic;
int picnum = 2;
unitsize(pic,1cm);

// Draw the triangle
path transformed_triangle = rotate(-240, center)* triangle;  // - is clockwise
filldraw(pic, transformed_triangle, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_triangle, 2), 2*unit(-point(transformed_triangle, 2)), p);
label(pic, "{\tiny $b$}", point(transformed_triangle, 1), 2*unit(-point(transformed_triangle, 1)), p);
label(pic, "{\tiny $c$}", point(transformed_triangle, 0), 2*unit(-point(transformed_triangle, 0)), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(triangle, 2), 1*N, p);
label(pic, "{\tiny $b$}", point(triangle, 1), 1*unit(point(triangle, 1)), p);
label(pic, "{\tiny $c$}", point(triangle, 0), 1*unit(point(triangle, 0)), p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ========= flip, f =========================
picture pic;
int picnum = 3;
unitsize(pic,1cm);

// Draw the triangle
path transformed_triangle = reflect((0,0), center)* triangle;  // 
filldraw(pic, transformed_triangle, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_triangle, 2), 2*unit(-point(transformed_triangle, 2)), p);
label(pic, "{\tiny $b$}", point(transformed_triangle, 1), 2*unit(-point(transformed_triangle, 1)), p);
label(pic, "{\tiny $c$}", point(transformed_triangle, 0), 2*unit(-point(transformed_triangle, 0)), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(triangle, 2), 1*N, p);
label(pic, "{\tiny $b$}", point(triangle, 1), 1*unit(point(triangle, 1)), p);
label(pic, "{\tiny $c$}", point(triangle, 0), 1*unit(point(triangle, 0)), p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ========= ft =========================
picture pic;
int picnum = 4;
unitsize(pic,1cm);

// Draw the triangle
path transformed_triangle = reflect((0,0), center)* rotate(-120, center)* triangle;  // 
filldraw(pic, transformed_triangle, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_triangle, 2), 2*unit(-point(transformed_triangle, 2)), p);
label(pic, "{\tiny $b$}", point(transformed_triangle, 1), 2*unit(-point(transformed_triangle, 1)), p);
label(pic, "{\tiny $c$}", point(transformed_triangle, 0), 2*unit(-point(transformed_triangle, 0)), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(triangle, 2), 1*N, p);
label(pic, "{\tiny $b$}", point(triangle, 1), 1*unit(point(triangle, 1)), p);
label(pic, "{\tiny $c$}", point(triangle, 0), 1*unit(point(triangle, 0)), p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ========= ft^2 =========================
picture pic;
int picnum = 5;
unitsize(pic,1cm);

// Draw the triangle
path transformed_triangle = reflect((0,0), center)* rotate(-240, center)* triangle;  // 
filldraw(pic, transformed_triangle, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_triangle, 2), 2*unit(-point(transformed_triangle, 2)), p);
label(pic, "{\tiny $b$}", point(transformed_triangle, 1), 2*unit(-point(transformed_triangle, 1)), p);
label(pic, "{\tiny $c$}", point(transformed_triangle, 0), 2*unit(-point(transformed_triangle, 0)), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(triangle, 2), 1*N, p);
label(pic, "{\tiny $b$}", point(triangle, 1), 1*unit(point(triangle, 1)), p);
label(pic, "{\tiny $c$}", point(triangle, 0), 1*unit(point(triangle, 0)), p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ========= square =========================
picture pic;
int picnum = 6;
unitsize(pic,1cm);

// Draw the square
path square = square_path(sidelength);
filldraw(pic, square, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(square, 0), SE, p);
label(pic, "{\tiny $b$}", point(square, 1), SW, p);
label(pic, "{\tiny $c$}", point(square, 2), NW, p);
label(pic, "{\tiny $d$}", point(square, 3), NE, p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(square, 0), NW, p);
label(pic, "{\tiny $b$}", point(square, 1), NE, p);
label(pic, "{\tiny $c$}", point(square, 2), SE, p);
label(pic, "{\tiny $d$}", point(square, 3), SW, p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ========= turn on square =========================
picture pic;
int picnum = 7;
unitsize(pic,1cm);

// Draw the square
path square = square_path(sidelength);
path transformed_square = rotate(-90) * square;
filldraw(pic, square, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_square, 0), -point(transformed_square,0), p);
label(pic, "{\tiny $b$}", point(transformed_square, 1), -point(transformed_square,1), p);
label(pic, "{\tiny $c$}", point(transformed_square, 2), -point(transformed_square,2), p);
label(pic, "{\tiny $d$}", point(transformed_square, 3), -point(transformed_square,3), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(square, 0), NW, p);
label(pic, "{\tiny $b$}", point(square, 1), NE, p);
label(pic, "{\tiny $c$}", point(square, 2), SE, p);
label(pic, "{\tiny $d$}", point(square, 3), SW, p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ========= turn^2 on square =========================
picture pic;
int picnum = 8;
unitsize(pic,1cm);

// Draw the square
path square = square_path(sidelength);
path transformed_square = rotate(-180) * square;
filldraw(pic, square, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_square, 0), -point(transformed_square,0), p);
label(pic, "{\tiny $b$}", point(transformed_square, 1), -point(transformed_square,1), p);
label(pic, "{\tiny $c$}", point(transformed_square, 2), -point(transformed_square,2), p);
label(pic, "{\tiny $d$}", point(transformed_square, 3), -point(transformed_square,3), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(square, 0), NW, p);
label(pic, "{\tiny $b$}", point(square, 1), NE, p);
label(pic, "{\tiny $c$}", point(square, 2), SE, p);
label(pic, "{\tiny $d$}", point(square, 3), SW, p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ========= turn^3 on square =========================
picture pic;
int picnum = 9;
unitsize(pic,1cm);

// Draw the square
path square = square_path(sidelength);
path transformed_square = rotate(-270) * square;
filldraw(pic, square, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_square, 0), -point(transformed_square,0), p);
label(pic, "{\tiny $b$}", point(transformed_square, 1), -point(transformed_square,1), p);
label(pic, "{\tiny $c$}", point(transformed_square, 2), -point(transformed_square,2), p);
label(pic, "{\tiny $d$}", point(transformed_square, 3), -point(transformed_square,3), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(square, 0), NW, p);
label(pic, "{\tiny $b$}", point(square, 1), NE, p);
label(pic, "{\tiny $c$}", point(square, 2), SE, p);
label(pic, "{\tiny $d$}", point(square, 3), SW, p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ========= flip on square =========================
picture pic;
int picnum = 10;
unitsize(pic,1cm);

// Draw the square
path square = square_path(sidelength);
path transformed_square = reflect((0,0), (0,1)) * square;
filldraw(pic, square, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_square, 0), -point(transformed_square,0), p);
label(pic, "{\tiny $b$}", point(transformed_square, 1), -point(transformed_square,1), p);
label(pic, "{\tiny $c$}", point(transformed_square, 2), -point(transformed_square,2), p);
label(pic, "{\tiny $d$}", point(transformed_square, 3), -point(transformed_square,3), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(square, 0), NW, p);
label(pic, "{\tiny $b$}", point(square, 1), NE, p);
label(pic, "{\tiny $c$}", point(square, 2), SE, p);
label(pic, "{\tiny $d$}", point(square, 3), SW, p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ========= f t on square =========================
picture pic;
int picnum = 11;
unitsize(pic,1cm);

// Draw the square
path square = square_path(sidelength);
path transformed_square = reflect((0,0), (0,1))*rotate(-90)* square;
filldraw(pic, square, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_square, 0), -point(transformed_square,0), p);
label(pic, "{\tiny $b$}", point(transformed_square, 1), -point(transformed_square,1), p);
label(pic, "{\tiny $c$}", point(transformed_square, 2), -point(transformed_square,2), p);
label(pic, "{\tiny $d$}", point(transformed_square, 3), -point(transformed_square,3), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(square, 0), NW, p);
label(pic, "{\tiny $b$}", point(square, 1), NE, p);
label(pic, "{\tiny $c$}", point(square, 2), SE, p);
label(pic, "{\tiny $d$}", point(square, 3), SW, p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ========= f t^2 on square =========================
picture pic;
int picnum = 12;
unitsize(pic,1cm);

// Draw the square
path square = square_path(sidelength);
path transformed_square = reflect((0,0), (0,1))*rotate(-180)* square;
filldraw(pic, square, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_square, 0), -point(transformed_square,0), p);
label(pic, "{\tiny $b$}", point(transformed_square, 1), -point(transformed_square,1), p);
label(pic, "{\tiny $c$}", point(transformed_square, 2), -point(transformed_square,2), p);
label(pic, "{\tiny $d$}", point(transformed_square, 3), -point(transformed_square,3), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(square, 0), NW, p);
label(pic, "{\tiny $b$}", point(square, 1), NE, p);
label(pic, "{\tiny $c$}", point(square, 2), SE, p);
label(pic, "{\tiny $d$}", point(square, 3), SW, p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ========= f t^3 on square =========================
picture pic;
int picnum = 13;
unitsize(pic,1cm);

// Draw the square
path square = square_path(sidelength);
path transformed_square = reflect((0,0), (0,1))*rotate(-270)* square;
filldraw(pic, square, palegreen, p);
// Label the inside corners 
label(pic, "{\tiny $a$}", point(transformed_square, 0), -point(transformed_square,0), p);
label(pic, "{\tiny $b$}", point(transformed_square, 1), -point(transformed_square,1), p);
label(pic, "{\tiny $c$}", point(transformed_square, 2), -point(transformed_square,2), p);
label(pic, "{\tiny $d$}", point(transformed_square, 3), -point(transformed_square,3), p);
// Label the outside corners 
label(pic, "{\tiny $a$}", point(square, 0), NW, p);
label(pic, "{\tiny $b$}", point(square, 1), NE, p);
label(pic, "{\tiny $c$}", point(square, 2), SE, p);
label(pic, "{\tiny $d$}", point(square, 3), SW, p);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


